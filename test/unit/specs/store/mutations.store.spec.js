import Vue from 'vue'
import Vuex from 'vuex'
import mutations from '@/store/mutations'
import * as mocks from '../mocks'

Vue.use(Vuex)

describe('Vuex mutations :', () => {
  it('calls GET_SONDBOX', () => {
    const state = {
      sondbox: new mocks.Sondbox()
    }
    mutations.GET_SONDBOX(state, new mocks.Sondbox('http://', false, false))
    expect(state.sondbox.media_url).to.equal('http://')
    expect(state.sondbox.recording_is_on).to.equal(false)
  })
  it('calls SAVE_SONDBOX', () => {
    const state = {
      sondbox: new mocks.Sondbox('http://', false, false, {id: 'action_1', value: 0})
    }
    mutations.SAVE_SONDBOX(state, {
      action: 'action_2',
      value: 1
    })
    expect(state.sondbox['action_2'].action).to.equal('action_2')
    expect(state.sondbox['action_2'].value).to.equal(1)
  })
})
