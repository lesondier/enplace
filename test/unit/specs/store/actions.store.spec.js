import sinon from 'sinon'
import testAction from '../utils'

const actions = {
  getSondbox: sinon.stub(),
  saveSondbox: sinon.stub()
}

describe('Vuex actions :', () => {
  it('calls getSondbox', (done) => {
    testAction(actions.getSondbox, [], {}, [
      { type: 'GET_SONDBOX' }
    ], done)
    done()
    expect(actions.getSondbox.calledOnce).to.equal(true)
  })
  it('calls saveSondbox', (done) => {
    testAction(actions.saveSondbox, [], {}, [
      { type: 'SAVE_SONDBOX' }
    ], done)
    done()
    expect(actions.getSondbox.calledOnce).to.equal(true)
  })
})
