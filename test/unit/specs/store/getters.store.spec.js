import Vuex from 'vuex'
import { mount } from 'avoriaz'

import getters from '@/store/getters'
import LsCard from '@/components/ls-card'
import * as mocks from '../mocks'

describe('Vuex getters :', () => {
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        sondbox: new mocks.Sondbox('http://', false, false, {
          id: 'action_1',
          value: 1
        })
      },
      getters
    })
  })
  it('should return a whole sondbox object', () => {
    const wrapper = mount(LsCard, { store })
    expect(wrapper.vm.$store.getters.getSondbox().media_url).to.equal('http://')
  })
  it('should return a related action object', () => {
    const wrapper = mount(LsCard, { store })
    expect(wrapper.vm.$store.getters.getSondboxParam('action_1').id).to.equal('action_1')
    expect(wrapper.vm.$store.getters.getSondboxParam('action_1').value).to.equal(1)
  })
})
