import _ from 'underscore'

export class Sondbox {
  constructor (mediaUrl, recordingIsOn, streamingIsOn, action) {
    this.media_url = mediaUrl
    this.recording_is_on = recordingIsOn
    this.streaming_is_on = streamingIsOn
    if (_.isObject(action)) {
      this[action.id] = {
        id: action.id || null,
        value: action.value || null,
        list_param: action.list_param || []
      }
    }
  }
}
