import Vuex from 'vuex'
import { shallow } from 'avoriaz'

import LsCard from '@/components/ls-card'
import getters from '@/store/getters'
import * as mocks from '../mocks'

describe('ls-card.vue', () => {
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        sondbox: new mocks.Sondbox('http://', false, false)
      },
      getters: getters
    })
  })
  it('should render correct contents', () => {
    const wrapper = shallow(LsCard, {
      attachToDocument: true,
      store,
      getters
    })
    expect(wrapper.isVueComponent).to.equal(true)
    expect(wrapper.vm.sondbox.media_url).to.equal('http://')
    expect(wrapper.vm.sondbox.streaming_is_on).to.equal(false)
    expect(wrapper.find('h1')[0].text()).to.equal('Sondbox')
    expect(wrapper.find('strong')[0].text()).to.equal('false')
  })
})
