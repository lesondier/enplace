import Vuex from 'vuex'
import { shallow } from 'avoriaz'

import LsSelect from '@/components/ls-select'
import getters from '@/store/getters'
import * as mocks from '../mocks'

describe('ls-select.vue', () => {
  let store
  beforeEach(() => {
    store = new Vuex.Store({
      state: {
        sondbox: new mocks.Sondbox('http://', false, false, {
          id: 'action_id',
          value: 1,
          list_param: ['item_1', 'item_2', 'item_3']
        })
      },
      getters
    })
  })
  it('should render correct contents', () => {
    const wrapper = shallow(LsSelect, {
      attachToDocument: true,
      store
    })
    wrapper.setProps({action: 'action_id'});
    wrapper.setProps({title: 'Action'});
    expect(wrapper.isVueComponent).to.equal(true)
    expect(wrapper.vm.getSondboxParam('action_id').id).to.equal('action_id')
    expect(wrapper.vm.getSondboxParam('action_id').value).to.equal(1)
    expect(wrapper.vm.getSondboxParam('action_id').list_param.length).to.equal(3)
    expect(wrapper.find('p')[0].text()).to.equal('Action')
    expect(wrapper.find('option')[0].text()).to.equal('item_1')
  })
})
