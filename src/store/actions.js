import SondboxService from '@/api/sondbox.service'

export default {
  getSondbox (context, channel) {
    SondboxService.getSondbox(channel && channel.id).then(sondbox => {
      context.commit('GET_SONDBOX', sondbox)
    })
  },
  saveSondbox (context, channel) {
    if (!channel) return
    SondboxService.saveSondbox(channel.id, channel.value).then(sondbox => {
      context.commit('SAVE_SONDBOX', sondbox)
    })
  }
}
