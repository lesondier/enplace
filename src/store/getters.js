export default {
  getSondbox: (state, getters) => () => {
    return state.sondbox
  },
  getSondboxParam: (state, getters) => (channel) => {
    return state.sondbox[channel]
  }
}
