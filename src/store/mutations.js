import * as types from './mutation-types'

export default {
  [types.GET_SONDBOX] (state, sondbox) {
    state.sondbox = sondbox
  },
  [types.SAVE_SONDBOX] (state, sondbox) {
    state.sondbox[sondbox.channel] = sondbox
  }
}
