import Vue from 'vue'
import VueResource from 'vue-resource'

Vue.use(VueResource)
Vue.http.options.root = window.location.origin + ':8080'

const SondboxResource = Vue.resource('audio{/channel}/')

export default {
  getSondbox (channel) {
    return SondboxResource.get({channel: channel}).then(sondbox => {
      return sondbox.body
    }, result => {
      console.log('error', result)
    })
  },
  saveSondbox (channel, value) {
    return SondboxResource.save({channel: channel}, {value: value}).then(sondbox => {
      return sondbox.body
    }, result => {
      console.log('error', result)
    })
  }
}
