import Vue from 'vue'
import Router from 'vue-router'
import GeneralSettings from '@/views/general-settings'

Vue.use(Router)

export default new Router({
  hashbang: false,
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'generalSettings',
      component: GeneralSettings
    }
  ]
})
