import Vue from 'vue'
import App from './App'
import router from './router/index.route'
import store from './store/store'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  created () {
    store.dispatch('getSondbox')
  },
  render: h => h(App)
})

